# Copyright 2016 Joseba García Etxebarria <joseba.gar@gmail.com>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


import math
from PyQt5 import QtWidgets
from Handler import Handler
from PyQt5.QtCore import QPointF, Qt
from PyQt5.QtGui import QPainter, QFont
from PyQt5.QtChart import QChart, QChartView, QValueAxis, QLineSeries


class STSHandler(Handler):
    """STS handler.
    This class should be able to parse STS files, as well as prepare the Qt4 UI for plotting,
    and include methods for replotting"""
    def __init__(self, path, tab_widget, parse=True, create_ui=True, plot=True):
        super(STSHandler, self).__init__(path, tab_widget)
        self.data = {}
        self.type = 'STS'

        if parse:
            self.parse()

        if create_ui:
            self.createUI()

        if plot:
            self.plot()

    def parse(self):
        """Parse the given file"""
        # The data type and field widths for each section
        field_widths = (9, 9, 6, 6, 5, 8, 8, 8, 6, 7, 11, 12, 11, 11, 13, 5)
        field_types = ('subcase', 'inc', 'icycl', 'isepa',
                       'icut', 'acycl', 'asplit', 'asepa',
                       'cut', 'rmesh', 'itime', 'ttime',
                       'wallt', 'cputime', 'maxresp',
                       'resptype')

        # Inititalize dictionaries
        for field in field_types:
            self.data[field] = []

        # Read the analysis progress file
        with open(self.path) as STSFile:
            read = False
            for line in STSFile:
                fields = line.split()
                # Read the header
                if '|--of the inc--|-----------of the analysis-----------|' in line:
                    read = True
                # Header read, read data
                # Also, make sure there's data to read
                elif len(fields) > 0 and read:
                    data_line = True
                    try:
                        int(fields[0])
                    except ValueError:
                        data_line = False

                    if data_line:
                        # Line has numerica data we can use
                        pos = 0
                        for i in range(len(field_widths)):
                            width = field_widths[i]
                            value = self.to_float_or_not(line[pos:pos + width].strip())
                            self.data[field_types[i]].append(value)
                            pos += width

                    else:
                        if line.strip().startswith('Job ends with exit number :'):
                            # Get the job's exit code
                            if fields[-1].strip() == '0':
                                self.failed = False
                            elif fields[-1].strip() == '1':
                                self.failed = True

    def plot(self):
        """Do whatever is needed for plotting the available data"""
        x = [100*float(data) for data in self.data["ttime"]]
        if len(x) < 1:
            return

        if max(x) < 10:
            x_max = 10*max(x)
        else:
            x_max = 100*math.ceil(max(x)/100)

        # TODO: Find the way to avoid clearing the vector each time
        # Analysis progress tab
        self.seriesProgress.clear()
        if len(self.data["wallt"]) > 0:
            y = self.data["wallt"]
            for i in range(len(x)):
                self.seriesProgress.append(QPointF(x[i], y[i]))

            self.axisXProgress.setRange(0, x_max)
            self.axisYProgress.setRange(0, 1.2*max(y))

        # Increment evolution tab
        self.seriesIncEvolution.clear()
        if len(self.data["inc"]) > 0:
            y = self.data["itime"]
            for i in range(len(x)):
                self.seriesIncEvolution.append(QPointF(x[i], y[i]))

            self.axisXIncEvolution.setRange(0, x_max)
            self.axisYIncEvolution.setRange(0, 1.2 * max(y))

        # Increment time tab
        self.seriesIncTime.clear()
        if len(self.data["inc"]) > 1 and len(self.data["wallt"]) > 0:
            y = [self.data["wallt"][i+1]-self.data["wallt"][i] for i in range(len(self.data["wallt"])-1)]

            for i in range(len(y)):
                self.seriesIncTime.append(QPointF(i, y[i]))

            self.axisXIncTime.setRange(0, i)
            self.axisYIncTime.setRange(0, 1.2 * max(y))

    def createUI(self):
        """Create the tabs within parent QTabWidget"""
        title_font = QFont()
        title_font.setPointSize(17)

        ################################
        # First tab: Analysis progress #
        ################################
        self.analysisProgress = QtWidgets.QWidget()
        self.analysisProgress.setObjectName("analysisProgress")
        self.STSverticalLayout_2 = QtWidgets.QVBoxLayout(self.analysisProgress)
        self.STSverticalLayout_2.setObjectName("STSverticalLayout_2")

        self.seriesProgress = QLineSeries()
        self.seriesProgress.setName('seriesProgress')

        self.chartProgress = QChart()
        self.chartProgress.legend().setVisible(False)
        self.chartProgress.addSeries(self.seriesProgress)
        self.chartProgress.setAnimationOptions(QChart.SeriesAnimations)

        self.axisXProgress = QValueAxis()
        self.axisXProgress.setTitleText('Analysis progress (%)')
        # self.axisXProgress.setMinorTickCount(6)
        self.axisXProgress.setTickCount(11)
        self.chartProgress.addAxis(self.axisXProgress, Qt.AlignBottom)
        self.seriesProgress.attachAxis(self.axisXProgress)

        self.axisYProgress = QValueAxis()
        self.axisYProgress.setTitleText('Wall time (s)')
        # self.axisYProgress.setMinorTickCount(6)
        self.axisYProgress.setTickCount(6)
        self.chartProgress.addAxis(self.axisYProgress, Qt.AlignLeft)
        self.seriesProgress.attachAxis(self.axisYProgress)

        self.chartProgress.setTitleFont(title_font)
        self.chartProgress.setTitle('Total analysis time (s)')

        self.chartViewProgress = QChartView(self.chartProgress)
        self.chartViewProgress.setRenderHint(QPainter.Antialiasing)
        self.chartViewProgress.setObjectName("plotProgress")

        self.STSverticalLayout_2.addWidget(self.chartViewProgress)
        self.tabWidget.addTab(self.analysisProgress, "Analysis progress")
        self.chartViewProgress.mouseDoubleClickEvent = self.double_click_handler

        #######################################
        # Second tab: Evolution of increments #
        #######################################
        self.incEvolution = QtWidgets.QWidget()
        self.incEvolution.setObjectName("incEvolution")
        self.STSverticalLayout = QtWidgets.QVBoxLayout(self.incEvolution)
        self.STSverticalLayout.setObjectName("STSverticalLayout")

        self.seriesIncEvolution = QLineSeries()
        self.seriesIncEvolution.setName('seriesIncEvolution')

        self.chartIncEvolution = QChart()
        self.chartIncEvolution.legend().setVisible(False)
        self.chartIncEvolution.addSeries(self.seriesIncEvolution)
        self.chartIncEvolution.setAnimationOptions(QChart.SeriesAnimations)

        self.axisXIncEvolution = QValueAxis()
        self.axisXIncEvolution.setTitleText('Analysis progress (%)')
        # self.axisXIncEvolution.setMinorTickCount(6)
        self.axisXIncEvolution.setTickCount(11)
        self.chartIncEvolution.addAxis(self.axisXIncEvolution, Qt.AlignBottom)
        self.seriesIncEvolution.attachAxis(self.axisXIncEvolution)

        self.axisYIncEvolution = QValueAxis()
        self.axisYIncEvolution.setTitleText('Increment')
        # self.axisYIncEvolution.setMinorTickCount(6)
        self.axisYIncEvolution.setTickCount(6)
        self.chartIncEvolution.addAxis(self.axisYIncEvolution, Qt.AlignLeft)
        self.seriesIncEvolution.attachAxis(self.axisYIncEvolution)

        self.chartIncEvolution.setTitleFont(title_font)
        self.chartIncEvolution.setTitle('Job increment evolution')

        self.chartViewIncEvolution = QChartView(self.chartIncEvolution)
        self.chartViewIncEvolution.setRenderHint(QPainter.Antialiasing)
        self.chartViewIncEvolution.setObjectName("plotProgress")

        self.STSverticalLayout.addWidget(self.chartViewIncEvolution)
        self.tabWidget.addTab(self.incEvolution, "Evolution of increments")
        self.chartViewIncEvolution.mouseDoubleClickEvent = self.double_click_handler

        ##############################
        # Third tab: Increment Times #
        ##############################
        self.incTime = QtWidgets.QWidget()
        self.incTime.setObjectName("incTime")
        self.STSverticalLayout_3 = QtWidgets.QVBoxLayout(self.incTime)
        self.STSverticalLayout_3.setObjectName("STSverticalLayout_3")

        self.seriesIncTime = QLineSeries()
        self.seriesIncTime.setName('seriesIncTime')

        self.chartIncTime = QChart()
        self.chartIncTime.legend().setVisible(False)
        self.chartIncTime.addSeries(self.seriesIncTime)
        self.chartIncTime.setAnimationOptions(QChart.SeriesAnimations)

        self.axisXIncTime = QValueAxis()
        # self.axisXIncTime.setTitleText('')
        # self.axisXIncTime.setMinorTickCount(6)
        self.axisXIncTime.setTickCount(11)
        self.chartIncTime.addAxis(self.axisXIncTime, Qt.AlignBottom)
        self.seriesIncTime.attachAxis(self.axisXIncTime)

        self.axisYIncTime = QValueAxis()
        self.axisYIncTime.setTitleText('Increment wall time (s)')
        # self.axisYIncTime.setMinorTickCount(6)
        self.axisYIncTime.setTickCount(6)
        self.chartIncTime.addAxis(self.axisYIncTime, Qt.AlignLeft)
        self.seriesIncTime.attachAxis(self.axisYIncTime)

        self.chartIncTime.setTitleFont(title_font)
        self.chartIncTime.setTitle('Job increment evolution')

        self.chartViewIncTime = QChartView(self.chartIncTime)
        self.chartViewIncTime.setRenderHint(QPainter.Antialiasing)
        self.chartViewIncTime.setObjectName("plotProgress")

        self.STSverticalLayout_3.addWidget(self.chartViewIncTime)
        self.tabWidget.addTab(self.incTime, "Increment times")
        self.chartViewIncTime.mouseDoubleClickEvent = self.double_click_handler

if __name__ == '__main__':
    path = 'test/nonlinear_LC3.sts'
    reader = STSHandler(path, None, True, False, False)
    print(reader.data)

    print(reader.failed)
# Long-running job monitor

This repository contains a very simple real time monitoring tool for long-running jobs written in Python3+PyQt5.

Right now it can read Nastran SOL 400 STS & Abaqus STA files, but the code can serve as a template for adding other kinds of jobs by creating new handlers which subclass [Handler](Handler.py). Since those subclasses are responsible for creating and destroying their own widget contents, you have a certain flexibility on what kind of data they can display.
You can have a look at [STAHandler.py](STAHandler.py) and [STSHandler.py](STSHandler.py) to understand how they work and use them as templates for adding support for your own tools.

The graphs will update real-time as the job progresses and when it finishes, the tool will display whether the job was successful or not in its status bar.

[[_toc_]]

# Screenshots

## Nastran
![Nastran SOL400 analysis progress](imgs/analysis_progress_sol400.png)
![Nastran SOL400 evolution of increments](imgs/evolution_of_increments_sol400.png)
![Nastran SOL400 time to compute each increment](imgs/increment_times_sol400.png)

## Abaqus
![Abaqus evolution of increments](imgs/evolution_of_increments_abaqus.png)
![Abaqus evolution of monitor DOF](imgs/dof_monitor_abaqus.png)

# Other features
It sports a funny monkey as its icon.

# Related projects
If you find this project useful, you might want to have a look at my [nastranModelReader](https://bitbucket.org/josebagar/nastranmodelreader) repo.

# Improvements
I might update the code to PyQtCharts.

Drop me a line at joseba.gar@gmail.com if you find the code useful!

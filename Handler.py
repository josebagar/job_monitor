# Copyright 2016 Joseba García Etxebarria <joseba.gar@gmail.com>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


import os
import sys
from PyQt5 import QtWidgets


class Handler(object):
    """A generic file handler class. Implements some default methods that should be overrided."""
    def __init__(self, path, tab_widget):
        self.path = path
        self.tabWidget = tab_widget
        self.failed = None
        self.type = None

    def parse(self):
        raise NotImplementedError('parse method not implemented')

    def plot(self):
        raise NotImplementedError('plot method not implemented')

    def createUI(self):
        raise NotImplementedError('createUI method not implemented')

    # The following two methods do actually provide useful utilities
    # By default, all the tabs will be closed, but you might want to do some other stuff
    def clear_ui(self):
        """Close all the tabs and clear their layouts"""
        # We need to reverse the order, otherwise one tab won't be closed
        for i in reversed(range(self.tabWidget.count())):
            self.tabWidget.setCurrentIndex(i)
            layout = self.tabWidget.currentWidget().layout()
            self.clear_layout(layout)
            self.tabWidget.removeTab(i)

    def clear_layout(self, layout):
        """Clear all the elements in a QLayout"""

        for i in reversed(range(layout.count())):
            item = layout.itemAt(i)

            if isinstance(item, QtWidgets.QWidgetItem):
                item.widget().deleteLater()
            else:
                self.clear_layout(item.layout())

            # remove the item from layout
            layout.removeItem(item)

    def double_click_handler(self, params):
        """Open the file in the Windows explorer"""
        if os.path.isfile(self.path) and sys.platform.startswith('win'):
            os.startfile(self.path)

    @staticmethod
    def to_float_or_not(value):
        """If the given value can be converted to a float number, return the converted
        value.
        Empty strings also get converted to 0. The restof values are returned as they were."""

        if value == '':
            return 0.0
        try:
            return float(value)
        except ValueError:
            return value


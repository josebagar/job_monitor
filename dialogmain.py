#!/usr/bin/env python

# Copyright 2016 Joseba García Etxebarria <joseba.gar@gmail.com>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


import os
import sys
from datetime import datetime as dt
# Import Qt modules
from PyQt5 import QtCore, QtWidgets

# Import the compiled UI module
from ui_mainwindow import Ui_MainWindow
# Import the STA reader and plotter
from STAHandler import STAHandler
from STSHandler import STSHandler


# Create a class for our main window
class DialogMain(QtWidgets.QMainWindow):
    def __init__(self):
        QtWidgets.QMainWindow.__init__(self)

        # This is always the same
        self.ui = Ui_MainWindow()
        self.ui.setupUi(self)
        self.ui.fileWatch = QtCore.QFileSystemWatcher(self)
        self.ui.timer = QtCore.QTimer(self)

        # Some attributes
        self.analysisFailed = None

        # Connect signals
        self.ui.actionOpenLog.triggered.connect(self.dialog_open_file)
        self.ui.actionQuit.triggered.connect(sys.exit)
        self.ui.fileWatch.fileChanged.connect(self.re_read)  # This won't work on network drives
        self.ui.timer.timeout.connect(self.re_read)          # So we also implement this (HACK!!!)
        self.ui.statusBarLabel = QtWidgets.QLabel()
        self.ui.statusBarLabel.setObjectName("label")
        self.ui.statusbar.insertPermanentWidget(0, self.ui.statusBarLabel)

        # Some defines
        self.fileHandler = None
        self.currentPath = None
        self.mtime = None
        if len(sys.argv) == 2:
            self.open_file(sys.argv[1])
        else:
            self.dialog_open_file()

    def open_file(self, path):
        # Clear stuff and store required variables
        self.ui.statusBarLabel.clear()
        self.currentPath = path
        # Add the new path to the fileWatch
        if len(self.ui.fileWatch.files()) > 0:
            self.ui.fileWatch.removePaths(self.ui.fileWatch.files())
        self.setWindowTitle('Job progress monitor - {}'.format(os.path.basename(path)))
        try:
            # Clear the UI, if needed
            if self.fileHandler:
                self.fileHandler.clear_ui()
            del self.fileHandler

            # Create the file handler object
            if path.lower().endswith('.sts'):
                self.fileHandler = STSHandler(path, self.ui.tabWidget)
            elif path.lower().endswith('.sta'):
                self.fileHandler = STAHandler(path, self.ui.tabWidget)
            else:
                QtWidgets.QMessageBox.critical(self, 'Could not open file',
                                               'The given file is not supported, sorry :(')
        except IOError:
            QtWidgets.QMessageBox.critical(self, 'Could not open file',
                                           'The given file cannot be opened, sorry :(')
        except NotImplementedError:
            QtWidgets.QMessageBox.critical(self, 'Could not open file',
                                           'The given file cannot be understood, sorry :(')

        # Set the timer and the QFileWatch, to detect file changes and react accordingly
        self.ui.fileWatch.addPath(path)
        if not self.ui.timer.isActive():
            self.ui.timer.start(15*1000)

        # Update the status bar message
        self.update_status_bar()

    def update_status_bar(self):
        """Update the status bar message"""
        # Display status message, if needed
        if self.fileHandler.failed is not None:
            msg = 'Analysis Completed'
            if self.fileHandler.failed:
                msg = 'Analysis Failed'
        else:
            msg = 'Analysis not finished'

        # Add a "last updated" message
        msg += ' - Last update: '+dt.now().strftime('%H:%M')

        self.ui.statusBarLabel.setText(msg)

    def re_read(self):
        """Re-Read log file and replot it if it has changed"""
        if os.path.isfile(self.currentPath):
            mtime = os.stat(self.currentPath).st_mtime
            if mtime != self.mtime:
                self.fileHandler.parse()
                self.fileHandler.plot()
                self.mtime = mtime
                self.update_status_bar()

    def dialog_open_file(self):
        """Show open file dialog and open it"""
        # Open File dialog should be pointed to the dir holding the current file
        dirname = ''
        if self.currentPath:
            dirname = os.path.dirname(self.currentPath)
        path, type = QtWidgets.QFileDialog.getOpenFileName(self,
                                                           'Choose monitor file',
                                                           dirname,
                                                           'Nastran STS (*.sts);;Abaqus STA (*.sta)')
        if path:
            self.open_file(str(path))
        else:
            if not self.currentPath:
                sys.exit()


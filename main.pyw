#!/usr/bin/env python3

# Copyright 2016 Joseba García Etxebarria <joseba.gar@gmail.com>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


"""This is the main program file for my job analysis monitor"""

import sys

# Import Qt modules
from PyQt5 import QtWidgets

# Other dialogs
from dialogmain import DialogMain


def main():
    # Again, this is boilerplate, it's going to be the same on
    # almost every app you write
    app = QtWidgets.QApplication(sys.argv)
    window = DialogMain()
    window.show()

    # It's exec_ because exec is a reserved word in Python
    sys.exit(app.exec_())


if __name__ == "__main__":
    main()

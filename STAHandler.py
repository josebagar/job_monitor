# Copyright 2016 Joseba García Etxebarria <joseba.gar@gmail.com>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


import math
from Handler import Handler
from PyQt5 import QtWidgets
from matplotlibwidget import MatplotlibWidget


class STAHandler(Handler):
    """STA handler.
    This method should be able to parse STA files, as well as prepare the Qt4 UI for plotting,
    and include methods for replotting"""
    def __init__(self, path, tab_widget, parse=True, create_ui=True, plot=True):
        super(STAHandler, self).__init__(path, tab_widget)
        self.sections = []
        self.type = 'STA'

        if parse:
            self.parse()

        if create_ui:
            self.createUI()

        if plot:
            self.plot()

    def parse(self):
        """Parse the given file"""
        # The data type and field widths for each section
        field_widths = {
            'INC': (5, 6, 3, 1, 7, 6, 6, 9, 11, 12, 10, 4),
            'CYCLE': (5, 8, 5, 5, 5, 11, 11, 11, 11)
        }

        field_types = {
            'INC': ("stp", "inc", "att", "converged", "seviters", "equiliters",
                    "titers", "ttime", "stime", "itime", "dofm", "riks"),
            'CYCLE': ("stp", "cycle", "iter", "inc", "att",
                      "ttime", "stime", "itime", "dofm")
            }

        # Read the analysis progress file
        with open(self.path) as STAFile:
            widths = None
            analysis_type = None
            for line in STAFile:
                fields = line.split()
                # Read the header
                if line.startswith(' STEP'):
                    analysis_type = fields[1]
                    if analysis_type not in field_widths.keys():
                        raise NotImplementedError('Cannot read STA file for analysis ' +
                                                  'type {}'.format(analysis_type))
                    widths = field_widths[analysis_type]
                    self.sections.append({})
                    # Inititalize dictionaries
                    for field in field_types[analysis_type]:
                        self.sections[-1][field] = []
                # Header read, read data
                # Also, make sure there's data to read
                elif widths is not None:
                    if len(fields) > 0:
                        data_line = True
                        try:
                            int(fields[0])
                        except ValueError:
                            data_line = False

                        if data_line:
                            # Line has numerical data we can use
                            pos = 0
                            for i in range(len(widths)):
                                width = widths[i]
                                value = self.to_float_or_not(line[pos:pos + width].strip())
                                self.sections[-1][field_types[analysis_type][i]].append(value)
                                pos += width
                        else:
                            if line.strip().startswith('THE ANALYSIS'):
                                # Get the job's exit code
                                if 'SUCCESSFULLY' in line:
                                    self.failed = False
                                else:
                                    self.failed = True

    def plot(self):
        """Do whatever is needed for plotting the available data"""
        x = [100*float(data) for data in self.sections[-1]["ttime"]]
        if max(x) < 10:
            x_max = 10*max(x)
        else:
            x_max = 100*math.ceil(max(x)/100)

        # Increment evolution tab
        if len(self.sections[-1]["inc"]) > 1:
            y = self.sections[-1]["itime"]
            self.plotIncEvolution.axes.hold(False)
            self.plotIncEvolution.axes.plot(x, y)
            self.plotIncEvolution.axes.set_xlim(0, x_max)
            self.plotIncEvolution.axes.set_ylim(0, 1.2*max(y))
            self.plotIncEvolution.axes.set_title("Job increment evolution")
            self.plotIncEvolution.axes.set_xlabel('Analysis progress (%)')
            self.plotIncEvolution.axes.set_ylabel('Increment')
            self.plotIncEvolution.axes.grid(which='major', axis='both')
            self.plotIncEvolution.draw()

        # DOF monitor tab
        if len(self.sections[-1]["dofm"]) > 0:
            y = [y_i for y_i in self.sections[-1]["dofm"]]
            self.plotDOFMonitor.axes.hold(False)
            self.plotDOFMonitor.axes.plot(x, y)
            self.plotDOFMonitor.axes.set_xlim(0, x_max)
            self.plotDOFMonitor.axes.set_ylim(0, 1.2*max(y))
            self.plotDOFMonitor.axes.set_title('Degree of freedom')
            self.plotDOFMonitor.axes.set_xlabel('Analysis progress (%)')
            self.plotDOFMonitor.axes.set_ylabel('DOF')
            self.plotDOFMonitor.axes.grid(which='major', axis='both')
            self.plotDOFMonitor.draw()

    def createUI(self):
        """Create the tabs within parent QTabWidget"""
        self.incEvolution = QtWidgets.QWidget()
        self.incEvolution.setObjectName("incEvolution")
        self.STSverticalLayout = QtWidgets.QVBoxLayout(self.incEvolution)
        self.STSverticalLayout.setObjectName("STSverticalLayout")
        self.plotIncEvolution = MatplotlibWidget(self.incEvolution)
        self.plotIncEvolution.setObjectName("plotIncEvolution")
        self.STSverticalLayout.addWidget(self.plotIncEvolution)
        self.tabWidget.addTab(self.incEvolution, 'Evolution of increments')
        self.plotIncEvolution.mouseDoubleClickEvent = self.double_click_handler

        self.DOFMonitor = QtWidgets.QWidget()
        self.DOFMonitor.setObjectName("DOFMonitor")
        self.STSverticalLayout_3 = QtWidgets.QVBoxLayout(self.DOFMonitor)
        self.STSverticalLayout_3.setObjectName("STSverticalLayout_3")
        self.plotDOFMonitor = MatplotlibWidget(self.DOFMonitor)
        self.plotDOFMonitor.setObjectName("plotDOFMonitor")
        self.STSverticalLayout_3.addWidget(self.plotDOFMonitor)
        self.tabWidget.addTab(self.DOFMonitor, "DOF Monitor")
        self.plotDOFMonitor.mouseDoubleClickEvent = self.double_click_handler

if __name__ == '__main__':
    path = 'test/test_cohesive3_damp.sta'
    path = 'test/Propagating-crack-plane-strain.sta'
    reader = STAHandler(path)
    for section in reader.sections:
        print(section)